﻿using FortCodeExercises.Excersice1;
using FortCodeExercises.Exercise1;
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            DisplayNewBulldozer();
            DisplayNewCrane();
            DisplayNewTractor();
            DisplayNewTruck();
            DisplayNewCar();

        }
        private static void DisplayNewBulldozer()
        {
            Bulldozer bulldozer = new Factory<Bulldozer>().Create();
            Console.WriteLine("Name: " + bulldozer.Info.Name);
            Console.WriteLine("Description: " + bulldozer.Info.Description);
            Console.WriteLine("Color: " + bulldozer.Info.Color);
            Console.WriteLine("Trim Color: " + bulldozer.Info.TrimColor);
        }

        private static void DisplayNewCrane()
        {
            Crane crane = new Factory<Crane>().Create();
            Console.WriteLine("Name: " + crane.Info.Name);
            Console.WriteLine("Description: " + crane.Info.Description);
            Console.WriteLine("Color: " + crane.Info.Color);
            Console.WriteLine("Trim Color: " + crane.Info.TrimColor);
        }
        private static void DisplayNewTractor()
        {
            Tractor tractor = new Factory<Tractor>().Create();
            Console.WriteLine("Name: " + tractor.Info.Name);
            Console.WriteLine("Description: " + tractor.Info.Description);
            Console.WriteLine("Color: " + tractor.Info.Color);
            Console.WriteLine("Trim Color: " + tractor.Info.TrimColor);
        }
        private static void DisplayNewTruck()
        {
            Truck truck = new Factory<Truck>().Create();
            Console.WriteLine("Name: " + truck.Info.Name);
            Console.WriteLine("Description: " + truck.Info.Description);
            Console.WriteLine("Color: " + truck.Info.Color);
            Console.WriteLine("Trim Color: " + truck.Info.TrimColor);
        }
        private static void DisplayNewCar()
        {
            Car car = new Factory<Car>().Create();
            Console.WriteLine("Name: " + car.Info.Name);
            Console.WriteLine("Description: " + car.Info.Description);
            Console.WriteLine("Color: " + car.Info.Color);
            Console.WriteLine("Trim Color: " + car.Info.TrimColor);
        }

    }
}
