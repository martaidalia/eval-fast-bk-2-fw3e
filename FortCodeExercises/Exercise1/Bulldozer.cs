﻿using FortCodeExercises.Excersice1;
using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public class Bulldozer : Machine
    {
        private int speed = 80;
        private bool hasMaxSpeed = true;
   
        public Bulldozer()
        {
            Init();

        }
        public override string GetColor()
        {
            return Constants.BulldozerConstants.Color;
        }
        public override string GetMachineName()
        {
            return Constants.BulldozerConstants.Name;
        }
        
        public void Init()
        {
            this.Speed = speed;
            this.HasMaxSpeed = hasMaxSpeed;
            this.Info = new MachineInfo();
            this.Info.Type = Enums.MachineType.Tractor;
            this.Info.Name = GetMachineName();
            this.Info.Color = GetColor();
            this.Info.Description = GetDescription(this.Info.Name, this.Info.Color, hasMaxSpeed);
        }
    }
}
