﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Excersice1
{
    public interface IMachine
    {
        string GetMachineName();
        string GetDescription(string name, string color, bool hasMaxSpeed);
        string GetColor();
        string GetTrimColor(string baseColor);
        bool IsDark(string color);
        int GetMaxSpeed(bool hasMaxSpeed = false);

    }
}
