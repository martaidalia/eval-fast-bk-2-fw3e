﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Enums
{
    /// <summary>
    /// Machine Types
    /// </summary>
    public enum MachineType
    {
        Bulldozer = 0,
        Crane = 1,
        Tractor = 2,
        Truck = 3,
        Car = 4
    }
}
