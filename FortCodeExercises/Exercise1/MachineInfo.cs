﻿using FortCodeExercises.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public class MachineInfo
    {
        public MachineType Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
        public string TrimColor { get; set; }
        public bool IsDark { get; set; }
        public int Max { get; set; }
    }
}
