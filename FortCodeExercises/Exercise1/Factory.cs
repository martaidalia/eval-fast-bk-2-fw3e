﻿using FortCodeExercises.Excersice1;
using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public class Factory<T> where T : Machine, new()
    {
        public T Create()
        {
            return new T();
        }
    }
}