﻿using FortCodeExercises.Excersice1;
using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public class Tractor : Machine
    {
        private int speed = 90;
        private bool hasMaxSpeed = true;
   
        public Tractor()
        {
            Init();

        }
        public override string GetColor()
        {
            return Constants.TractorConstants.Color;
        }
        public override string GetMachineName()
        {
            return Constants.TractorConstants.Name;
        }
        public override string GetTrimColor(string baseColor)
        {
            if (this.IsDark(baseColor))
                return Constants.TrimColors.Gold;
            return string.Empty;
        }
        public void Init()
        {
            this.Speed = speed;
            this.HasMaxSpeed = hasMaxSpeed;
            this.Info = new MachineInfo();
            this.Info.Type = Enums.MachineType.Tractor;
            this.Info.Name = GetMachineName();
            this.Info.Color = GetColor();
            this.Info.Description = GetDescription(this.Info.Name, this.Info.Color, hasMaxSpeed);
            this.Info.TrimColor = GetTrimColor(Constants.TractorConstants.BaseColor);
        }
    }
}
