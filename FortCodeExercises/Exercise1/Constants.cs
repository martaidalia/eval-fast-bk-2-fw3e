﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public static class Constants
    {
        public static class BulldozerConstants
        {
            public const string Name = "bulldozer";
            public const string Color = "red";
            public const string BaseColor = "red";
        }
        public static class CraneConstants
        {
            public const string Name = "crane";
            public const string Color = "blue";
            public const string BaseColor = "blue";
        }

        public static class TractorConstants
        {
            public const string Name = "tractor";
            public const string Color = "green";
            public const string BaseColor = "green";
        }
        public static class TruckConstants
        {
            public const string Name = "truck";
            public const string Color = "yellow";
            public const string BaseColor = "yellow";
        }
        public static class CarConstants
        {
            public const string Name = "car";
            public const string Color = "brown";
            public const string BaseColor = "brown";
        }
        public static class DefaultConstants
        {
            public const string Color = "white";
        }
        public static class DarkColors
        {
            public const string Green = "green";
            public const string Black = "black";
            public const string Crimson = "crimson";
            public const string Red = "red";
        }
        public static class TrimColors
        {
            public const string Gold = "gold";
            public const string White = "white";
            public const string Black = "black";
            public const string Silver = "silver";
        }
    }
}
