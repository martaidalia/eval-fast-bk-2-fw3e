﻿using System;
using System.Collections.Generic;
using System.Text;
using FortCodeExercises.Enums;
using FortCodeExercises.Excersice1;

namespace FortCodeExercises.Exercise1
{
    public abstract class Machine: IMachine
    {
        protected int Speed { get; set; }
        protected int AbsoluteMaxSpeed { get; set; } = 70;
        protected bool HasMaxSpeed { get; set; }
        public virtual MachineInfo Info { get; set; }
        public virtual string GetDescription(string name, string color, bool hasMaxSpeed = true)
        {
            StringBuilder description = new StringBuilder();
            description.Append(" ");
            description.Append(color);
            description.Append(" ");
            description.Append(name);
            description.Append(" [");
            description.Append(GetMaxSpeed(hasMaxSpeed));
            description.Append("].");
            return description.ToString();
        }


        public virtual string GetTrimColor(string baseColor)
        {
            return "";
        }


        public virtual string GetColor()
        {
            return Constants.DefaultConstants.Color;
        }

      
        public bool IsDark(string color)
        {
            return (color == Constants.DarkColors.Black ||
                  color == Constants.DarkColors.Crimson ||
                  color == Constants.DarkColors.Green ||
                  color == Constants.DarkColors.Red);
        }

        public virtual string GetMachineName()
        {
            return "";
        }

        public virtual int GetMaxSpeed(bool hasMaxSpeed = false)
        {
            return !hasMaxSpeed ? this.AbsoluteMaxSpeed : this.Speed;
        }
      
    }
}
