﻿I modified the code in the following manner:

Introduced and abstract class Machine, which has all of the functions that can be perform on a machine
Introduced and interface to be implemented by the Machine class that defines the contract for IMachine
In the future we could have IToyMachine or IWasherMachine which would define the function for different types of machines
I introduced a Factory class using generics that would create an object of type Machine
I then created a class for each type of the machines that inherit from Machine and overrride when needed 
the methods of Machine that are specific to the machine implementing the abstract class, i.e. Truc, Car, Crane are 
some of the classes implementing Machine
I introduced MachineINfo as th eproperty that holds the data for each machine type
I also introduced static contants classes to hold each of the specific attribues for each machine type as well as othe rneeded
constants to avoind hardcoding string
These changes allow you to introduced new machines in the future and being able to call MachineInfo to check the data 
on each of the machines
Each type knows how to initialiaze itself or you can call specific methods to get the data back like getcolor etc.
Thanks!